import { Estado } from '../../estado/entities/estado.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ type: 'varchar', length: 50, unique: true })
  username: string;
  @Column({ type: 'varchar', length: 50 })
  password: string;
  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;
  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
  @DeleteDateColumn({ name: 'deleted_at' })
  deletedAt: Date;

  @Column({ name: 'estado_id', default: 1 })
  estadoId: number;
  @ManyToOne(() => Estado, (estado) => estado.users)
  @JoinColumn({ name: 'estado_id' })
  estado: Estado;
}
