import { Injectable } from '@nestjs/common';
import { CreateEstadoDto } from './dto/create-estado.dto';
import { UpdateEstadoDto } from './dto/update-estado.dto';
import { Estado } from './entities/estado.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class EstadoService {
  constructor(
    @InjectRepository(Estado) private estadoRepository: Repository<Estado>,
  ) {}
  create(createEstadoDto: CreateEstadoDto) {
    return 'This action adds a new estado';
  }

  findAll() {
    return this.estadoRepository.find();
  }

  findOne(id: number) {
    return this.estadoRepository.findOne({
      where: {
        id,
      },
    });
  }

  update(id: number, updateEstadoDto: UpdateEstadoDto) {
    return `This action updates a #${id} estado`;
  }

  remove(id: number) {
    return `This action removes a #${id} estado`;
  }
}
