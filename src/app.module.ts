import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { config } from './config';
import { DatabaseConfig } from './database.config';
import { UserModule } from './user/user.module';
import { EstadoModule } from './estado/estado.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useClass: DatabaseConfig,
    }),
    UserModule,
    EstadoModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
