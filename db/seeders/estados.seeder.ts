import { Estado } from '../../src/estado/entities/estado.entity';
import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';

export class EstadoSeeder implements Seeder {
  track = false;

  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager,
  ): Promise<any> {
    const repository = dataSource.getRepository(Estado);
    await repository.insert([{ nombre: 'ALTA' }, { nombre: 'BAJA' }]);
  }
}
