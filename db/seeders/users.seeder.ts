import { User } from '../../src/user/entities/user.entity';
import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';

export class UserSeeder implements Seeder {
  track = false;

  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager,
  ): Promise<any> {
    const repository = dataSource.getRepository(User);
    await repository.insert([
      { username: 'admin', password: '123456' },
      { username: 'guest', password: '123456' },
      { username: 'user', password: '123456' },
    ]);
  }
}
